# Abstract code for building a directory hierarchy according to a DSL.
# 
# To use this, 
#   * Mix it into something
#   * Override at least template_dir_path, possibly also other methods in "Methods to override"
#   * Create a hierarchy_spec as specified by comment on make_hierarchy
#   * Create a value_hash of values to put into templates
#   * Invoke make_hierarchy
#
# For examples of use, see:
#   * MM - Uses gsub and NAME_OPERATIONS
#   * plugin.appGen - Uses ERB, NAME_OPERATIONS, and FILE_OPERATIONS


require 'fileutils'
require 'erb'

class String
  # Identical to capitalize but affects only the first character.
  def upcaseFirst
    return self[0,1].capitalize + self[1.. self.length]
  end

  # Identical to downcase but affects only the first character.
  def downcaseFirst
    return self[0,1].downcase + self[1.. self.length]
  end
end 


module Hierarchy_Builder
  # NOTE: declaring module_function allows methods defined in Hierarchy_Builder to be called
  #       as class methods. This allows backwards compatibility with the old style of including Hierarchy_Builder.
  module_function
  
  # Specifies absolute path to the template directory. You must set this.
  def template_dir_path=(value); @template_dir_path = value; end
  def template_dir_path; @template_dir_path || raise("You must set template_dir_path or template_dir_env!"); end
  
  # Specifies optional name of environmental variable used to hold the path of a directory used to
  # to hold per-user customizations of templates. You must set this if you want to use it.
  def template_dir_env=(value); @template_dir_env = value; end
  def template_dir_env; @template_dir_env; end
  
  # When non nil, ERB uses this to populate $SAFE
  def erb_safe_eval=(value); @erb_safe_eval = value; end
  def erb_safe_eval; @erb_safe_eval; end
  
  # When non nil, adjusts behior of ERB.  A String containing one or more of:
  #   %  enables Ruby code processing for lines beginning with %  (This can mess up HAML templates which are generated from ERB templates)
  #   <> omit newline for lines starting with <% and ending in %>
  #   >  omit newline for lines ending in %>
  def erb_trim_mode=(value); @erb_trim_mode = value; end
  def erb_trim_mode; @erb_trim_mode || "%<>"; end
  
  FILE_OPERATIONS = [:chmod, :chmod_R, :chown, :chown_R]
  NAME_OPERATIONS = [:upcaseFirst, :downcaseFirst, :downcase, :upcase]
  
  
  # Builds a directory hierarchy within root_dir, from a hierarchy_spec (DSL) based on Hashes and Symbols.
  # For an example, see create_project in  mm/projects.
  #
  # This functionality includes:
  #   * Names of files and directories that are either
  #        * Exactly specified within the DSL
  #        * Sybolically identified in the DSL, and resolved into names to be actually used (for example replacing :project_name by the actual name of the project)
  #   * Optionally modifying names (after resolution) by 
  #        * any unary operator, specified in NAME_OPERATIONS
  #        * a Proc
  #   * Copying files from templates:
  #        * Directly from a template directory in this project, or from a per-user override directory
  #        * Optionally processed by ERB
  #   * Optionally modifying file ownership and/or permissions by methods in FILE_OPERATIONS
  #
  # Here is a complete specification of the DSL.  The hierarchy_spec is required to be a Hash of the form: {item_name => item_spec}
  # item_name specifies the name of something specified by item_spec.
  # item_name can be:
  #   * a Symbol (used as a key to find the actual value in value_hash - value can be a path within root_dir instead of just a name)
  #   * a String (gsub is applied for each entry in value_hash - result can be a path within root_dir instead of just a name)
  #   * an Array with first value as above, and other values either symbols from NAME_OPERATIONS, or Procs.
  # item_spec can be:
  #   * nil - specifies creation of empty file
  #   * a Symbol - specifies a template to be copied *without* any substitutions (permissions & ownership *ARE* copied)
  #   * a String - specifies a template to be instantiated *with* substitutions  (permissions & ownership *NOT* copied)
  #   * a hierarchy_spec
  #   * an Array - specifies a template and further processing by one or more of the methods of FILE_OPERATIONS
  #     The Array must be of the form [aSymbol-or-String, aCommand, ... lastCommand ]
  #     where aSymbol-or-String specifies a template, and each subsequent command is an Array of the form [method-from-FILE_OPERATIONS, argument, ... last-argument]
  #     Any argument that is :FILE_PATH is replaced by the actual file name.  We could just append the file name at the end of the arguments, but that would
  #     make it harder to add new FILE_OPERATIONS
  #
  # The value_hash must be a hash. When an .erb template is instantiated, the hash is converted into a binding that contains the hash values as local variables.
  #
  # Templates are located by template_path(item_spec)
  def make_hierarchy(root_dir, hierarchy_spec, value_hash={})
    # puts ">>> make_hierarcy for hierarchy_spec: #{hierarchy_spec.inspect}"
    FileUtils.mkdir_p(root_dir) unless File.exist?(root_dir)
    FileUtils.cd(root_dir) {
      hierarchy_spec.each {|item_name, item_spec|
        make_item(item_name, item_spec, value_hash)
        }
      make_item('placeholder.txt', :'placeholder.txt') if hierarchy_spec.empty? && value_hash[:use_placeholders]
      }
  end
  
  # Make one file or directory, in the current directory
  # The exact behavior depends on item_spec (see description of item_spec in comment for make_hierarchy)
  # The file name or directory name is derived from item_name (see description of item_name in comment for make_hierarchy)
  def make_item(item_name, item_spec, value_hash={})
    target = resolve_name(item_name, value_hash)
    case item_spec
      when nil, Symbol, String, Proc
        _make_file(item_name, item_spec, target, value_hash)
      when Array
        template = item_spec.shift
        _make_file(item_name, template, target, value_hash)
        while command = item_spec.shift # This is NOT intended to be '=='
          raise "Command must be an Array" unless command.instance_of?(Array)
          command = command.collect {|arg| :FILE_PATH==arg ? target : arg }
          message = command.shift
          raise "Command message must be in #{FILE_OPERATIONS.inspect}, found #{message.inspect}" unless FILE_OPERATIONS.member?(message)
# puts "FILE_OPERATION: #{message.inspect} with arguments #{command.inspect}"; $stdout.flush
          FileUtils.send(message, *command)
        end
      when Hash
        # Make a directory
        FileUtils.mkdir_p target
        make_hierarchy(target, item_spec, value_hash)
      else raise "Unexpected item_spec type. Expected nil, String, Symbol, Proc, Array, or Hash, found #{item_spec.class.name}"
    end
  end
  
  # If item_name is a Symbol, look it up in value_hash.  If item_name is a String, perform gsub substitutions from value_hash
  def resolve_name(item_name, value_hash)
    # puts "RESOLVING item_name: #{item_name.inspect} with value_hash: #{value_hash.inspect}"
    case item_name
      when Symbol
        val = value_hash[item_name]
        raise "No value found in value_hash for heirarchy_spec item_name #{item_name.inspect}. Keys are #{value_hash.keys.inspect}." unless val
        val.to_s
      when String
        apply_gsub(item_name, value_hash)
      when Array
        raise "Arrays used to specify names must be of form [name, member-of-NAME_OPERATIONS-or-proc], but failed to have at least two items, was: #{item_name.inspect}" unless item_name.size>1
        # Do not understand why, but clone resulted in an empty array. This was not due to a typo in a variable name. Could not reproduce this problem in irb.
        operators = item_name[1..-1] # item_name.clone # need to work with a copy if the same hierarchy_spec is to be used more than once
        symbolic_name = item_name[0] # operators.shift
        name = resolve_name(symbolic_name, value_hash)
        operators.each {|operator|
          case operator
            when Symbol ; name = name.send operator
            when Proc   ; name = operator.call(name)
            else raise "Unexpected operator class. Expected Symbol or Proc, but found #{operator.class.name}"
          end
          }
         name
      else raise "Unexpected heirarchy_spec item_name type. Expected Symbol, String, or Array, found #{item_name.class.name}"
    end
  end
  
  # This case statement could be part of the case in make_hierarchy. This is factored out for use when item_spec is an Array
  def _make_file(item_name, template, target, value_hash)
    case template
      when nil
        # Make an empty file
        FileUtils.touch target
      when Symbol
        # Copy a template, without any subitutions
        path = template_path(template)
        raise "Unable to find template #{template} for item_name #{item_name}" unless path
        FileUtils.cp(path, target, :preserve=>true)
      when String
        # Instantiate a template
        path = template_path(template)
        raise "Unable to find template #{template} for item_name #{item_name}" unless path
        instantiate_template(path, target, value_hash)
      when Proc
        write_proc_output_to_file(template, target, value_hash)
      else raise "Unexpected template specification - should have been nil, Symbol, or String, found a #{template.class.name}"
    end
  end
  
  # It is usually easier to use make_item than this method, becuase make_item finds the template & does any post processing.
  # Instantiates at dest_path a template located at template_path  (both paths include the file name)
  # If value_hash is empty, this is just a copy.
  # If value_hash is non empty, the template is populated with data from the value_hash, as follows:
  # * When the template has extension .erb, 
  #   the value_hash is of the form { variable_name => variable_value }
  #   The values from the value_hash are set as locals in the binding: the ERB template can access them as through getters.
  # * When the template has extension .haml, raise an error (HAML is heavy and intended for HTML, ERB is more general and baked in)
  # * In all other cases, multiple gsubs are performed, one for each entry in value_hash: { pattern => replacement }
  def instantiate_template(template_path, dest_path, value_hash={})
    raise "Template not found (#{template_path})" unless File.exist?(template_path)
    return FileUtils.cp(template_path, dest_path) if value_hash.empty?
    ext = File.extname(template_path)
    template = File.read template_path
    case ext
      when '.erb'
        erb = ERB.new(template, erb_safe_eval, erb_trim_mode)
        b = binding
        value_hash.each { |key, value| eval("#{key} = value_hash[#{key.inspect}]", b) }
        body = erb.result(b)
        File.open( dest_path, 'w') {|f| f << body }
      when '.haml'
        raise "instantiate_template does not currently support #{ext}. It supports ERB and gsub.  It is intended to be lightweight."
      else
        body = apply_gsub(template, value_hash)
        File.open( dest_path, 'w') {|f| f << body }
    end
  end
  
  def write_proc_output_to_file(proc, target, value_hash={})
    output = apply_gsub(proc.call, value_hash)
    # NOTE: the 'b' (binary) file open mode is required for some setups.
    #       I wasn't able to replicate it in the spec tests, but the write 
    #       will fail with the error:
    #       Encoding::UndefinedConversionError:
    #         "\xFF" from ASCII-8BIT to UTF-8
    File.open(target, 'wb'){|f| f << output}
  end
  
  def apply_gsub(body, value_hash)
    body = body.to_s + '' # body.clone copies the frozen aspect of Hash keys
    value_hash.each { |key, value| 
      key = key.to_s unless key.instance_of?(Regexp) # do not replace Regexp
      body.gsub!(key, value.to_s) 
      }
    body
  end
  
  # Returns a path to a template. nil if no template could be found.
  # local_path is within the template directory. The following template directories are searched: 
  # * first in directory specified by environmental var MM_TEMPLATES (intended for customization)
  # * and then in mm/templates (intended for built-in default functionality)
  def template_path(local_path)
    # Check if local_path is already valid (absolute path).
    # Added this option to avoid issues with overriding methods to specify template directory. -SMD
    return local_path if File.exist?(local_path.to_s)
    
    # Then look in ENV[template_dir_env] (intended for customization)
    if template_dir_env
      path = File.join(ENV[Hierarchy_Builder.template_dir_env], local_path) if ENV[Hierarchy_Builder.template_dir_env]
      return path if path && File.exist?(path)
    end
    
    # Then look in template_dir_path (inteded for built in defaults)
    path = File.join(Hierarchy_Builder.template_dir_path, local_path.to_s)
    File.exist?(path) ? path : nil
  end
  
  # Takes a relative or absolute path to a file, and returns a proc of the file's contents
  def literal_file(path)
    # NOTE: relative_to_nth_caller gets the path relative to the calling file, not this file.
    relative_path = relative_to_nth_caller(path, 2)
    # Use path argument as an absolute path unless relative path interpretation exists.
    path = relative_path if File.exist?(relative_path)
    proc { File.binread(path) }
  end
  
  # Takes a relative or absolute path to a file, and returns a hash of the directory's contents (with files as procs)
  def literal_dir(path)
    dir_spec = {}
    # NOTE: relative_to_nth_caller gets the path relative to the calling file, not this file.
    relative_path = relative_to_nth_caller(path, 2)
    # Use path argument as an absolute path unless relative path interpretation exists.
    path = relative_path if File.exist?(relative_path)
    Dir[path + '/*'].each do |child|
      child_spec = File.directory?(child) ? literal_dir(child) : literal_file(child)
      dir_spec[File.basename(child)] = child_spec
    end
    dir_spec
  end
end