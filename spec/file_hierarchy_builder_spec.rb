require 'spec_helper'

describe "file hierarchy creation" do
  before :all do
    @target_dir = File.join(File.dirname(__FILE__), 'target_dir')
  end
  
  after :each do
    FileUtils.rm_rf(@target_dir) if File.exist?(@target_dir)
  end
  
  it "should create a basic file hierarchy" do
    # Make the dir structure:
    # test_project >
    #     barfile.rb (copied from bar.rb)
    Hierarchy_Builder.make_hierarchy(@target_dir, {'test_project' => {:foo => :'bar.rb'}}, {:foo => 'barfile.rb'})
    
    File.exist?(@target_dir).should == true
    test_project_dir = File.join(@target_dir, 'test_project')
    File.exist?(test_project_dir).should == true
    File.directory?(test_project_dir).should == true
    barfile = File.join(test_project_dir, 'barfile.rb')
    File.exist?(barfile).should == true
    File.directory?(barfile).should == false
    File.read(barfile).should == File.read(File.join(TEMPLATE_PATH, 'bar.rb'))
  end
  
  it "should allow the use of a proc to define a file's contents" do
    Hierarchy_Builder.make_hierarchy(@target_dir, {'test_project' => {'proc_defined.txt' => proc { 'test'.upcase } }}, {:foo => 'bar'})
    
    proc_defined_file = File.join(@target_dir, 'test_project', 'proc_defined.txt')
    File.exist?(proc_defined_file).should == true
    File.read(proc_defined_file).should == 'TEST'
  end
  
  it "should be able to create an empty file (via nil)" do
    Hierarchy_Builder.make_hierarchy(@target_dir, {'test_project' => {'empty.txt' => nil }})
    
    empty_file = File.join(@target_dir, 'test_project', 'empty.txt')
    File.exist?(empty_file).should == true
    File.read(empty_file).should == ''
  end
  
  it "should be able to handle binary data within a proc (using literal_file)" do
    picture_path = relative('test_files/picture.jpeg')
    picture_data = File.binread(picture_path)
    Hierarchy_Builder.make_hierarchy(@target_dir, {'test_project' => {'mypicture.jpg' => Hierarchy_Builder.literal_file('test_files/picture.jpeg')}}, {:foo => 'bar'})
    
    proc_defined_image = File.join(@target_dir, 'test_project', 'mypicture.jpg')
    File.exist?(proc_defined_image).should == true
    File.binread(proc_defined_image).length.should == picture_data.length
  end
  
  it "should be able to handle binary data within a proc (using literal_dir)" do
    picture_path = relative('test_files/picture.jpeg')
    picture_data = File.binread(picture_path)
    Hierarchy_Builder.make_hierarchy(@target_dir, {'test_project' => Hierarchy_Builder.literal_dir('test_files')}, {:foo => 'bar'})
    
    proc_defined_image = File.join(@target_dir, 'test_project', 'picture.jpeg')
    File.exist?(proc_defined_image).should == true
    File.binread(proc_defined_image).length.should == picture_data.length
  end
end